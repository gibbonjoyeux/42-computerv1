#!/usr/bin/python3

################################################################################
##[ IMPORTS ]###################################################################
################################################################################

import  re;

################################################################################
##[ MODULE ]####################################################################
################################################################################

##################################################
def parse(tokens): ###############################
    ############################## 
    ##[ PROPERTIES ]##############
    ############################## 
    equation = [0, 0, 0];
    i = 0;
    state = True;
    number = 1;

    ############################## 
    ##[ LOCAL FUNCTIONS ]#########
    ############################## 

    ############################## 
    def error(detail):
        print("ERROR: " + detail);
        exit(1);

    ############################## 
    def get_sign(number, sign):
        if (sign == "-"):
            return (-number);
        return (number);

    ############################## 
    def get_equation(tokens, equation, start, i):
        max = len(tokens);
        state = True;
        number = start;
        num_spe = False;
        state = True;
        while (True):
            num_spe = False;
            while (tokens[i]["type"] == "SIGN"):
                state = True;
                number = get_sign(number, tokens[i]["value"]);
                i += 1;
                if (i >= max):
                    error("The number is not specified after sign");
            if (state == False):
                error("The sign is not specified");
            if (tokens[i]["type"] == "NUMBER"):
                number *= tokens[i]["value"];
                num_spe = True;
                i += 1;
                if (i >= max):
                    if (start == 1):
                        error("The equation is malformed");
                    equation[0] += number;
                    break;
            if (tokens[i]["type"] == "OPERATOR"):
                if (start == 1 and tokens[i]["value"] == "="):
                    if (num_spe == False):
                        error("The number is not specified");
                    equation[0] += number;
                    break;
                if (tokens[i]["value"] != "*"):
                    error("Undefined character after number");
                i += 1;
                if (i >= max):
                    error("X is not specified");
            if (tokens[i]["type"] == "X"):
                i += 1;
                if (i >= max):
                    equation[1] += number;
                    break;
                if (tokens[i]["type"] == "OPERATOR"):
                    if (start == 1 and tokens[i]["value"] == "="):
                        equation[1] += number;
                        #print("-----" + str(equation) + "-----(4)");
                        break;
                    if (tokens[i]["value"] != "^"):
                        error("Undefined character after X");
                    i += 1;
                    if (i >= max):
                        error("X exponent is not specified");
                    if (tokens[i]["type"] != "NUMBER"):
                        error("X exponent is not specified");
                    if (tokens[i]["value"] != int(tokens[i]["value"])): 
                        error("X exponent must be an integer");
                    if (tokens[i]["value"] > 2):
                        error("X exponent must be between 0 and 2");
                    equation[int(tokens[i]["value"])] += number;
                    i += 1;
                    if (i >= max):
                        break;
                else:
                    equation[1] += number;
                    #print("-----" + str(equation) + "-----(6)");
            else:
                equation[0] += number;
            if (start == 1 and tokens[i]["type"] == "OPERATOR" and tokens[i]["value"] == "="):
                break;
            number = start;
            state = False;
        return (i);

    ############################## 
    ##[ MAIN ]####################
    ############################## 

    ##### LEFT PART  -> [... =] #####
    i = get_equation(tokens, equation, 1, 0);
    if (i + 1 >= len(tokens)):
        error("The equation is malformed");
    ##### RIGHT PART -> [= ...] #####
    get_equation(tokens, equation, -1, i + 1);
    return (equation);
